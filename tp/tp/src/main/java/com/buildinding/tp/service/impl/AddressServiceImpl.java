package com.buildinding.tp.service.impl;

import com.buildinding.tp.exception.ResourceNotFoundException;
import com.buildinding.tp.model.Address;
import com.buildinding.tp.repository.AddressRepository;
import com.buildinding.tp.service.AddressService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;

    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public Address create(Address address) {
        return this.addressRepository.save(address);
    }

    @Override
    public List<Address> getAll() {
        return this.addressRepository.findAll();
    }

    @Override
    public Address getById(Integer id) throws ResourceNotFoundException {
        return this.addressRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public Address update(Address address) {
        Address addressToUpdate = this.getById(address.getId());
        addressToUpdate.setCity(addressToUpdate.getCity());
        addressToUpdate.setStreet(address.getStreet());
        addressToUpdate.setStreetNb(address.getStreetNb());
        addressToUpdate.setInhabitants(address.getInhabitants());
        addressToUpdate.setWorksite(address.getWorksite());
        return this.addressRepository.save(addressToUpdate);
    }

    @Override
    public void delete(Integer id) {
        Address addressToDelete = this.getById(id);
        this.addressRepository.delete(addressToDelete);
    }
}
