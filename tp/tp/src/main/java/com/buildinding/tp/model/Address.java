package com.buildinding.tp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "adresses")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "address")
    private List<Technician> inhabitants;

    @OneToOne(mappedBy = "address")
    private Worksite worksite;

    private String city;

    private String street;

    @Column(name = "street_nb")
    private String streetNb;
}
