package com.buildinding.tp.api.v1;

import com.buildinding.tp.api.dto.WorksiteDto;
import com.buildinding.tp.exception.ResourceNotFoundException;
import com.buildinding.tp.mapper.WorksiteMapper;
import com.buildinding.tp.service.WorksiteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/worksites")
public class WorksiteApi {

    Logger log = LoggerFactory.getLogger(WorksiteApi.class);

    private final WorksiteService worksiteService;

    private final WorksiteMapper worksiteMapper;

    public WorksiteApi(WorksiteService worksiteService, WorksiteMapper worksiteMapper) {
        this.worksiteService = worksiteService;
        this.worksiteMapper = worksiteMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Get a list of all worksites")
    public ResponseEntity<List<WorksiteDto>> getAll() {
        return ResponseEntity.ok(
                worksiteService.getAll().stream()
                        .map(worksiteMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Get a worksite by its id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Return the worksite found for the given ID"),
            @ApiResponse(responseCode = "404", description = "No worksite found the given ID")
    })
    public ResponseEntity<WorksiteDto> getById(@PathVariable final Integer id) {
        try {
            return ResponseEntity.ok(worksiteMapper.mapToDto(worksiteService.getById(id)));
        } catch (ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create a new worksite")
    @ApiResponse(responseCode = "201", description = "Created")
    public ResponseEntity<WorksiteDto> create(@RequestBody WorksiteDto worksiteDto) {
        WorksiteDto worksiteDtoResponse = worksiteMapper.mapToDto(
                worksiteService.create(
                        worksiteMapper.mapToModel(worksiteDto)
                )
        );
        return ResponseEntity.created(URI.create("/v1/worksites/" + worksiteDtoResponse.getId()))
                .body(worksiteDtoResponse);
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Modify an existing worksite")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "404", description = "No worksite found the given ID")
    })
    public ResponseEntity<Void> update(@PathVariable final Integer id, @RequestBody WorksiteDto worksiteDto) {
        try {
            worksiteDto.setId(id);
            worksiteService.update(worksiteMapper.mapToModel(worksiteDto));
            return ResponseEntity.noContent().build();
        } catch (ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete a worksite for the given ID")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "404", description = "No worksite found for the given ID")
    })
    public ResponseEntity<Void> delete(@PathVariable final Integer id) {
        try {
            worksiteService.delete(id);
            return ResponseEntity.noContent().build();
        } catch(ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
