package com.buildinding.tp.exception;

public class ResourceNotFoundException extends RuntimeException{

    public ResourceNotFoundException() {
        super("Ressource non trouvée");
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }

}
