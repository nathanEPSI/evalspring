package com.buildinding.tp.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ManagerDto {
    private Integer id;
    private List<TechnicianDto> technicians;
    private String firstname;
    private String lastname;
    private String phone;
    private String mobilePhone;

}
