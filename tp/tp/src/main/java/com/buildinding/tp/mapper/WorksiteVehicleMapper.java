package com.buildinding.tp.mapper;

import com.buildinding.tp.api.dto.WorksiteVehicleDto;
import com.buildinding.tp.model.WorksiteVehicle;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface WorksiteVehicleMapper {

    @Mapping(source = "owner.id", target = "ownerId")
    WorksiteVehicleDto mapToDto(WorksiteVehicle worksiteVehicle);

    @Mapping(source = "ownerId", target = "owner.id")
    WorksiteVehicle mapToModel(WorksiteVehicleDto worksiteVehicleDto);

}
