package com.buildinding.tp.mapper;

import com.buildinding.tp.api.dto.AddressDto;
import com.buildinding.tp.model.Address;
import com.buildinding.tp.model.Technician;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface AddressMapper {

    @Mapping(source = "worksite.id", target = "worksiteId")
    @Mapping(target = "inhabitantIds", expression = "java(getInhabitantIds(address))")
    AddressDto mapToDto (Address address);

    default List<Integer> getInhabitantIds(Address address) {
        if(address.getInhabitants() != null) {
            return address.getInhabitants().stream().map(
                    Technician::getId
            ).toList();
        }
        return new ArrayList<>();
    }

    @Mapping(source = "worksiteId", target = "worksite.id")
    Address mapToModel (AddressDto addressDto);

}
