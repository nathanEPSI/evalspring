package com.buildinding.tp.service;

import com.buildinding.tp.model.WorksiteVehicle;

import java.util.List;

public interface WorksiteVehicleService {

    WorksiteVehicle create(WorksiteVehicle worksiteVehicle);

    List<WorksiteVehicle> getAll();

    WorksiteVehicle getById(Integer id);

    WorksiteVehicle update(WorksiteVehicle worksiteVehicle);

    void delete(Integer id);

}
