package com.buildinding.tp.repository;

import com.buildinding.tp.model.WorksiteVehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorksiteVehicleRepository extends JpaRepository<WorksiteVehicle, Integer> {
}
