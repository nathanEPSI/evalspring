package com.buildinding.tp.api.v1;

import com.buildinding.tp.api.dto.WorksiteVehicleDto;
import com.buildinding.tp.exception.ResourceNotFoundException;
import com.buildinding.tp.mapper.WorksiteVehicleMapper;
import com.buildinding.tp.service.WorksiteVehicleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/worksiteVehicles")
public class WorksiteVehicleApi {

    Logger log = LoggerFactory.getLogger(WorksiteVehicleApi.class);

    private final WorksiteVehicleService worksiteVehicleService;
    private final WorksiteVehicleMapper worksiteVehicleMapper;

    public WorksiteVehicleApi(WorksiteVehicleService worksiteVehicleService, WorksiteVehicleMapper worksiteVehicleMapper) {
        this.worksiteVehicleService = worksiteVehicleService;
        this.worksiteVehicleMapper = worksiteVehicleMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Get a list of all worksiteVehicles")
    public ResponseEntity<List<WorksiteVehicleDto>> getAll() {
        return ResponseEntity.ok(
                worksiteVehicleService.getAll().stream()
                        .map(worksiteVehicleMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Get a worksiteVehicle by its id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Return the worksiteVehicle found for the given ID"),
            @ApiResponse(responseCode = "404", description = "No worksiteVehicle found the given ID")
    })
    public ResponseEntity<WorksiteVehicleDto> getById(@PathVariable final Integer id) {
        try {
            return ResponseEntity.ok(worksiteVehicleMapper.mapToDto(worksiteVehicleService.getById(id)));
        } catch (ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create a new worksiteVehicle")
    @ApiResponse(responseCode = "201", description = "Created")
    public ResponseEntity<WorksiteVehicleDto> create(@RequestBody WorksiteVehicleDto worksiteVehicleDto) {
        WorksiteVehicleDto worksiteVehicleDtoResponse = worksiteVehicleMapper.mapToDto(
                worksiteVehicleService.create(
                        worksiteVehicleMapper.mapToModel(worksiteVehicleDto)
                )
        );
        return ResponseEntity.created(URI.create("/v1/worksiteVehicles/" + worksiteVehicleDtoResponse.getId()))
                .body(worksiteVehicleDtoResponse);
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Modify an existing worksiteVehicle")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "404", description = "No worksiteVehicle found the given ID")
    })
    public ResponseEntity<Void> update(@PathVariable final Integer id, @RequestBody WorksiteVehicleDto worksiteVehicleDto) {
        try {
            worksiteVehicleDto.setId(id);
            worksiteVehicleService.update(worksiteVehicleMapper.mapToModel(worksiteVehicleDto));
            return ResponseEntity.noContent().build();
        } catch (ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete a worksiteVehicle for the given ID")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "404", description = "No worksiteVehicle found for the given ID")
    })
    public ResponseEntity<Void> delete(@PathVariable final Integer id) {
        try {
            worksiteVehicleService.delete(id);
            return ResponseEntity.noContent().build();
        } catch(ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
