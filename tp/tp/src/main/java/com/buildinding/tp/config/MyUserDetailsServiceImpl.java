package com.buildinding.tp.config;

import com.buildinding.tp.exception.UnknowRessourceException;
import com.buildinding.tp.model.User;
import com.buildinding.tp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class MyUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       try {
           User existingUser = this.userService.getByUsername(username);
           return new MyUserDetails(existingUser);
       } catch (UnknowRessourceException ure){
           throw new UsernameNotFoundException("User with username " + username + " not found");
       }
    }
}
