package com.buildinding.tp.api.v1;

import com.buildinding.tp.api.dto.TechnicianDto;
import com.buildinding.tp.exception.ResourceNotFoundException;
import com.buildinding.tp.mapper.TechnicianMapper;
import com.buildinding.tp.service.TechnicianService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/technicians")
public class TechnicianApi {

    Logger log = LoggerFactory.getLogger(TechnicianApi.class);

    private final TechnicianService technicianService;
    private final TechnicianMapper technicianMapper;

    public TechnicianApi(TechnicianService technicianService, TechnicianMapper technicianMapper) {
        this.technicianService = technicianService;
        this.technicianMapper = technicianMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Get a list of all technicians")
    public ResponseEntity<List<TechnicianDto>> getAll() {
        return ResponseEntity.ok(
                technicianService.getAll().stream()
                        .map(technicianMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Get a technician by its id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Return the technician found for the given ID"),
            @ApiResponse(responseCode = "404", description = "No technician found the given ID")
    })
    public ResponseEntity<TechnicianDto> getById(@PathVariable final Integer id) {
        try {
            return ResponseEntity.ok(technicianMapper.mapToDto(technicianService.getById(id)));
        } catch (ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create a new technician")
    @ApiResponse(responseCode = "201", description = "Created")
    public ResponseEntity<TechnicianDto> create(@RequestBody TechnicianDto technicianDto) {
        TechnicianDto technicianDtoResponse = technicianMapper.mapToDto(
                technicianService.create(
                        technicianMapper.mapToModel(technicianDto)
                )
        );
        return ResponseEntity.created(URI.create("/v1/technicians/" + technicianDtoResponse.getId()))
                .body(technicianDtoResponse);
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Modify an existing technician")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "404", description = "No technician found the given ID")
    })
    public ResponseEntity<Void> update(@PathVariable final Integer id, @RequestBody TechnicianDto technicianDto) {
        try {
            technicianDto.setId(id);
            technicianService.update(technicianMapper.mapToModel(technicianDto));
            return ResponseEntity.noContent().build();
        } catch (ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete a technician for the given ID")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "404", description = "No technician found for the given ID")
    })
    public ResponseEntity<Void> delete(@PathVariable final Integer id) {
        try {
            technicianService.delete(id);
            return ResponseEntity.noContent().build();
        } catch(ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
