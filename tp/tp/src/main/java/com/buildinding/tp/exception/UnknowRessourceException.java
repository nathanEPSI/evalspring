package com.buildinding.tp.exception;

public class UnknowRessourceException extends RuntimeException{

    public UnknowRessourceException(){
        super("Ressource inconnu");
    }

    public UnknowRessourceException(String message){
        super(message);
    }

}
