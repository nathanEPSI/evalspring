package com.buildinding.tp.service;

import com.buildinding.tp.model.Manager;

import java.util.List;

public interface ManagerService {

    Manager create(Manager manager);

    List<Manager> getAll();

    Manager getById(Integer id);

    Manager update(Manager manager);

    void delete(Integer id);

}
