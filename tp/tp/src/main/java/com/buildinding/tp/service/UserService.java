package com.buildinding.tp.service;

import com.buildinding.tp.model.User;


public interface UserService {

    User getByUsername(String username);

    User getUserByUsernameAndPassword(String username, String password);
}
