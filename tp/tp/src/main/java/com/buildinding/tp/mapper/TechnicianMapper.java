package com.buildinding.tp.mapper;

import com.buildinding.tp.api.dto.TechnicianDto;
import com.buildinding.tp.model.Technician;
import com.buildinding.tp.model.Worksite;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface TechnicianMapper {

    @Mapping(source = "manager.id", target = "managerId")
    @Mapping(source = "address.id", target = "addressId")
    @Mapping(source = "worksiteVehicle.id", target = "worksiteVehicleId")
    @Mapping(target = "worksiteIds", expression = "java(getWorksites(technician))")
    TechnicianDto mapToDto(Technician technician);

    default List<Integer> getWorksites(Technician technician) {
        if (technician.getWorksites() != null) {
            return technician.getWorksites().stream().map(Worksite::getId).toList();
        }
        return new ArrayList<>();
    }


    @Mapping(source = "managerId", target = "manager.id")
    @Mapping(source = "addressId", target = "address.id")
    @Mapping(source = "worksiteVehicleId", target = "worksiteVehicle.id")
    Technician mapToModel(TechnicianDto technicianDto);
}
