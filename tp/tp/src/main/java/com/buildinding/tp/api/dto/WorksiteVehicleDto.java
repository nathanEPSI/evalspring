package com.buildinding.tp.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WorksiteVehicleDto {
    private Integer id;
    private String model;
    private Integer constructionYear;
    private Integer ownerId;
}
