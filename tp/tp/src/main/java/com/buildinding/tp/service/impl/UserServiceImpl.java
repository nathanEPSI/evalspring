package com.buildinding.tp.service.impl;

import com.buildinding.tp.exception.UnknowRessourceException;
import com.buildinding.tp.model.User;
import com.buildinding.tp.repository.UserRepository;
import com.buildinding.tp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User getByUsername(String username) {
        return this.userRepository.findByUsername(username).orElseThrow(() -> new UnknowRessourceException("No user found for this username."));
    }

    @Override
    public User getUserByUsernameAndPassword(String username, String password) {
        User user = this.userRepository.findByUsername(username).get();
        if (new BCryptPasswordEncoder().matches(password, user.getPassword())) {
            return user;
        }
        throw new UnknowRessourceException("No user found for the given user/password");
    }
}
