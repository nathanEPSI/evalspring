package com.buildinding.tp.service;

import com.buildinding.tp.model.Technician;

import java.util.List;

public interface TechnicianService {

    Technician create(Technician technician);

    List<Technician> getAll();

    Technician getById(Integer id);

    Technician update(Technician technician);

    void delete(Integer id);

}
