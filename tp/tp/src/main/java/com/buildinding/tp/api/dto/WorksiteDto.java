package com.buildinding.tp.api.dto;

import com.buildinding.tp.model.Address;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WorksiteDto {
    private Integer id;
    private String name;
    private Integer addressId;
    private List<Integer> techniciansId;
}
