package com.buildinding.tp.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TechnicianDto {

    private Integer id;
    private String firstname;
    private String lastname;
    private Integer age;
    private Integer managerId;
    private Integer addressId;
    private List<Integer> worksiteIds;
    private Integer worksiteVehicleId;

}
