package com.buildinding.tp.mapper;

import com.buildinding.tp.api.dto.ManagerDto;
import com.buildinding.tp.api.dto.TechnicianDto;
import com.buildinding.tp.model.Manager;
import com.buildinding.tp.model.Technician;
import com.buildinding.tp.model.Worksite;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface ManagerMapper {

    @Mapping(target = "technicians", expression = "java(getTechnicians(manager))")
    ManagerDto mapToDto (Manager manager);

    default List<TechnicianDto> getTechnicians(Manager manager) {
        if (manager.getTechnicians() != null) {
            return manager.getTechnicians().stream().map(
                    technician -> new TechnicianDto(
                            technician.getId(),
                            technician.getFirstname(),
                            technician.getLastname(),
                            technician.getAge(),
                            manager.getId(),
                            technician.getAddress().getId(),
                            technician.getWorksites().stream().map(Worksite::getId).toList(),
                            technician.getWorksiteVehicle().getId()
                    )
            ).toList();
        }
        return new ArrayList<>();
    }

    Manager mapToModel (ManagerDto managerDto);

}
