package com.buildinding.tp.service.impl;

import com.buildinding.tp.exception.ResourceNotFoundException;
import com.buildinding.tp.model.Manager;
import com.buildinding.tp.repository.ManagerRepository;
import com.buildinding.tp.service.ManagerService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
public class ManagerServiceImpl implements ManagerService {

    private final ManagerRepository managerRepository;

    public ManagerServiceImpl(ManagerRepository managerRepository) {
        this.managerRepository = managerRepository;
    }

    @Override
    public Manager create(Manager manager) {
        return this.managerRepository.save(manager);
    }

    @Override
    public List<Manager> getAll() {
        return this.managerRepository.findAll();
    }

    @Override
    public Manager getById(Integer id) throws ResourceNotFoundException {
        return this.managerRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public Manager update(Manager manager) {
        Manager managerToUpdate = this.getById(manager.getId());
        managerToUpdate.setFirstname(manager.getFirstname());
        managerToUpdate.setLastname(manager.getLastname());
        managerToUpdate.setPhone(manager.getPhone());
        managerToUpdate.setMobilePhone(manager.getMobilePhone());
        managerToUpdate.setTechnicians(manager.getTechnicians());
        return this.managerRepository.save(managerToUpdate);
    }

    @Override
    public void delete(Integer id) {
        Manager managerToDelete = this.getById(id);
        this.managerRepository.delete(managerToDelete);
    }
}
