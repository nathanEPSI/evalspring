package com.buildinding.tp.api.v1;

import com.buildinding.tp.api.dto.AddressDto;
import com.buildinding.tp.exception.ResourceNotFoundException;
import com.buildinding.tp.mapper.AddressMapper;
import com.buildinding.tp.service.AddressService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/v1/addresses")
public class AddressApi {

    Logger log = LoggerFactory.getLogger(AddressApi.class);

    private AddressService addressService;
    private AddressMapper addressMapper;

    public AddressApi(AddressService addressService, AddressMapper addressMapper) {
        this.addressService = addressService;
        this.addressMapper = addressMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Get a list of all addresses")
    public ResponseEntity<List<AddressDto>> getAll() {
        return ResponseEntity.ok(
                addressService.getAll().stream()
                        .map(address -> addressMapper.mapToDto(address))
                        .toList()
        );
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Get an address by its id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Return the address found for the given ID"),
            @ApiResponse(responseCode = "404", description = "No address found the given ID")
    })
    public ResponseEntity<AddressDto> getById(@PathVariable final Integer id) {
        try {
            return ResponseEntity.ok(addressMapper.mapToDto(addressService.getById(id)));
        } catch (ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create a new address")
    @ApiResponse(responseCode = "201", description = "Created")
    public ResponseEntity<AddressDto> create(@RequestBody AddressDto addressDto) {
        AddressDto addressDtoResponse = addressMapper.mapToDto(
                addressService.create(
                        addressMapper.mapToModel(addressDto)
                )
        );
        return ResponseEntity.created(URI.create("/v1/addresses/" + addressDtoResponse.getId()))
                .body(addressDtoResponse);
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Modify an existing address")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "404", description = "No address found the given ID")
    })
    public ResponseEntity<Void> update(@PathVariable final Integer id, @RequestBody AddressDto addressDto) {
        try {
            addressDto.setId(id);
            addressService.update(addressMapper.mapToModel(addressDto));
            return ResponseEntity.noContent().build();
        } catch (ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete an address for the given ID")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "404", description = "No address found for the given ID")
    })
    public ResponseEntity<Void> delete(@PathVariable final Integer id) {
        try {
            addressService.delete(id);
            return ResponseEntity.noContent().build();
        } catch(ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
