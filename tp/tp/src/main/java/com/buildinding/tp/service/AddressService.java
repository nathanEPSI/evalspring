package com.buildinding.tp.service;

import com.buildinding.tp.model.Address;

import java.util.List;

public interface AddressService {

    Address create(Address address);

    List<Address> getAll();

    Address getById(Integer id);

    Address update(Address address);

    void delete(Integer id);

}
