package com.buildinding.tp.service.impl;

import com.buildinding.tp.exception.ResourceNotFoundException;
import com.buildinding.tp.model.WorksiteVehicle;
import com.buildinding.tp.repository.WorksiteVehicleRepository;
import com.buildinding.tp.service.WorksiteVehicleService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
public class WorksiteVehicleImpl implements WorksiteVehicleService {

    private final WorksiteVehicleRepository worksiteVehicleRepository;

    public WorksiteVehicleImpl(WorksiteVehicleRepository worksiteVehicleRepository) {
        this.worksiteVehicleRepository = worksiteVehicleRepository;
    }

    @Override
    public WorksiteVehicle create(WorksiteVehicle worksiteVehicle) {
        return this.worksiteVehicleRepository.save(worksiteVehicle);
    }

    @Override
    public List<WorksiteVehicle> getAll() {
        return this.worksiteVehicleRepository.findAll();
    }

    @Override
    public WorksiteVehicle getById(Integer id) throws ResourceNotFoundException {
        return this.worksiteVehicleRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public WorksiteVehicle update(WorksiteVehicle worksiteVehicle) {
        WorksiteVehicle wvToUpdate = this.getById(worksiteVehicle.getId());
        wvToUpdate.setModel(worksiteVehicle.getModel());
        wvToUpdate.setConstructionYear(worksiteVehicle.getConstructionYear());
        wvToUpdate.setOwner(worksiteVehicle.getOwner());
        return this.worksiteVehicleRepository.save(wvToUpdate);
    }

    @Override
    public void delete(Integer id) {
        WorksiteVehicle wvToDelete = this.getById(id);
        this.worksiteVehicleRepository.delete(wvToDelete);
    }
}
