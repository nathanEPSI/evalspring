package com.buildinding.tp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "worksite_vehicles")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WorksiteVehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String model;

    @Column(name = "construction_year")
    private Integer constructionYear;

    @OneToOne(mappedBy = "worksiteVehicle")
    private Technician owner;

}
