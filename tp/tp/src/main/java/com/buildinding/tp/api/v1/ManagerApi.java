package com.buildinding.tp.api.v1;

import com.buildinding.tp.api.dto.ManagerDto;
import com.buildinding.tp.exception.ResourceNotFoundException;
import com.buildinding.tp.mapper.ManagerMapper;
import com.buildinding.tp.service.ManagerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/managers")
public class ManagerApi {

    Logger log = LoggerFactory.getLogger(ManagerApi.class);

    private ManagerService managerService;
    private ManagerMapper managerMapper;

    public ManagerApi(ManagerService managerService, ManagerMapper managerMapper) {
        this.managerService = managerService;
        this.managerMapper = managerMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Get a list of all managers")
    public ResponseEntity<List<ManagerDto>> getAll() {
        return ResponseEntity.ok(
                managerService.getAll().stream()
                        .map(manager -> managerMapper.mapToDto(manager))
                        .toList()
        );
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Get a manager by its id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Return the manager found for the given ID"),
            @ApiResponse(responseCode = "404", description = "No manager found the given ID")
    })
    public ResponseEntity<ManagerDto> getById(@PathVariable final Integer id) {
        try {
            return ResponseEntity.ok(managerMapper.mapToDto(managerService.getById(id)));
        } catch (ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create a new manager")
    @ApiResponse(responseCode = "201", description = "Created")
    public ResponseEntity<ManagerDto> create(@RequestBody ManagerDto managerDto) {
        ManagerDto managerDtoResponse = managerMapper.mapToDto(
                managerService.create(
                        managerMapper.mapToModel(managerDto)
                )
        );
        return ResponseEntity.created(URI.create("/v1/managers/" + managerDtoResponse.getId()))
                .body(managerDtoResponse);
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Modify an existing manager")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "404", description = "No manager found the given ID")
    })
    public ResponseEntity<Void> update(@PathVariable final Integer id, @RequestBody ManagerDto managerDto) {
        try {
            managerDto.setId(id);
            managerService.update(managerMapper.mapToModel(managerDto));
            return ResponseEntity.noContent().build();
        } catch (ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete a manager for the given ID")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "404", description = "No manager found for the given ID")
    })
    public ResponseEntity<Void> delete(@PathVariable final Integer id) {
        try {
            managerService.delete(id);
            return ResponseEntity.noContent().build();
        } catch(ResourceNotFoundException rnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
