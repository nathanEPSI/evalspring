package com.buildinding.tp.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {
    private Integer id;
    private List<Integer> inhabitantIds;
    private Integer worksiteId;
    private String city;
    private String street;
    private String streetNb;
}
