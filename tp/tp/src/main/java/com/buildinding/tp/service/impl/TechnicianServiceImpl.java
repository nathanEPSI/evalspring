package com.buildinding.tp.service.impl;

import com.buildinding.tp.exception.ResourceNotFoundException;
import com.buildinding.tp.model.Technician;
import com.buildinding.tp.repository.TechnicianRepository;
import com.buildinding.tp.service.TechnicianService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
public class TechnicianServiceImpl implements TechnicianService {

    private final TechnicianRepository technicianRepository;

    public TechnicianServiceImpl(TechnicianRepository technicianRepository) {
        this.technicianRepository = technicianRepository;
    }

    @Override
    public Technician create(Technician technician) {
        return this.technicianRepository.save(technician);
    }

    @Override
    public List<Technician> getAll() {
        return this.technicianRepository.findAll();
    }

    @Override
    public Technician getById(Integer id) throws ResourceNotFoundException {
        return this.technicianRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public Technician update(Technician technician) {
        Technician technicianToUpdate = this.getById(technician.getId());
        technicianToUpdate.setFirstname(technician.getFirstname());
        technicianToUpdate.setLastname(technician.getLastname());
        technicianToUpdate.setAge(technician.getAge());
        technicianToUpdate.setAddress(technician.getAddress());
        technicianToUpdate.setManager(technician.getManager());
        technicianToUpdate.setWorksites(technician.getWorksites());
        technicianToUpdate.setWorksiteVehicle(technician.getWorksiteVehicle());
        return this.technicianRepository.save(technicianToUpdate);
    }

    @Override
    public void delete(Integer id) {
        Technician technicianToDelete = this.getById(id);
        this.technicianRepository.delete(technicianToDelete);
    }
}
