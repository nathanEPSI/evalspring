package com.buildinding.tp.service;

import com.buildinding.tp.model.Worksite;

import java.util.List;

public interface WorksiteService {

    Worksite create(Worksite worksite);

    List<Worksite> getAll();

    Worksite getById(Integer id);

    Worksite update(Worksite worksite);

    void delete(Integer id);

}
