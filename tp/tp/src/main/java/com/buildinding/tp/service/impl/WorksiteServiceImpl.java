package com.buildinding.tp.service.impl;

import com.buildinding.tp.exception.ResourceNotFoundException;
import com.buildinding.tp.model.Address;
import com.buildinding.tp.model.Technician;
import com.buildinding.tp.model.Worksite;
import com.buildinding.tp.repository.AddressRepository;
import com.buildinding.tp.repository.TechnicianRepository;
import com.buildinding.tp.repository.WorksiteRepository;
import com.buildinding.tp.service.WorksiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Primary
@Transactional
public class WorksiteServiceImpl implements WorksiteService {

    private final WorksiteRepository worksiteRepository;

    private final AddressRepository addressRepository;

    private final TechnicianRepository technicianRepository;

    public WorksiteServiceImpl(WorksiteRepository worksiteRepository, AddressRepository addressRepository, TechnicianRepository technicianRepository) {
        this.worksiteRepository = worksiteRepository;
        this.addressRepository = addressRepository;
        this.technicianRepository = technicianRepository;
    }

    @Override
    public Worksite create(Worksite worksite) {
        worksite.setAddress(this.addressRepository.getById(worksite.getAddress().getId()));
        worksite.setTechnicians(worksite.getTechnicians().stream().map(this.technicianRepository::save).toList());
        return this.worksiteRepository.save(worksite);
    }

    @Override
    public List<Worksite> getAll() {
        return this.worksiteRepository.findAll();
    }

    @Override
    public Worksite getById(Integer id) throws ResourceNotFoundException {
        return this.worksiteRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public Worksite update(Worksite worksite) {
        Worksite worksiteToUpdate = this.getById(worksite.getId());
        worksiteToUpdate.setName(worksite.getName());
        worksiteToUpdate.setAddress(worksite.getAddress());
        worksiteToUpdate.setTechnicians(worksite.getTechnicians());
        return worksiteRepository.save(worksiteToUpdate);
    }

    @Override
    public void delete(Integer id) {
        Worksite worksiteToDelete = this.getById(id);
        this.worksiteRepository.delete(worksiteToDelete);
    }
}
