package com.buildinding.tp.mapper;

import com.buildinding.tp.api.dto.TechnicianDto;
import com.buildinding.tp.api.dto.WorksiteDto;
import com.buildinding.tp.model.Technician;
import com.buildinding.tp.model.Worksite;
import com.buildinding.tp.service.TechnicianService;
import com.buildinding.tp.service.impl.TechnicianServiceImpl;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface WorksiteMapper {
    @Mapping(source = "address.id", target = "addressId")
    @Mapping(target = "techniciansId", expression = "java(getTechnicianIds(worksite))")
    WorksiteDto mapToDto(Worksite worksite);

    default List<Integer> getTechnicianIds(Worksite worksite) {
        if (worksite.getTechnicians() != null) {
            return worksite.getTechnicians().stream().map(Technician::getId).toList();
        }
        return new ArrayList<>();
    }


    @Mapping(source = "addressId", target = "address.id")
    @Mapping(target = "technicians", expression = "java(getTechnicians(worksiteDto))")
    Worksite mapToModel(WorksiteDto worksiteDto);

    default List<Technician> getTechnicians(WorksiteDto worksiteDto) {
        if (worksiteDto.getTechniciansId() != null) {
            return worksiteDto.getTechniciansId().stream().map(
                    Technician::new
                    ).toList();
        }
        return new ArrayList<>();
    }

}
